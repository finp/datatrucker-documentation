---
sidebar_position: 1
---
<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->

## Prerequisites

### Supported OS

*   Ubuntu
*   RHEL
*   Centos
*   Ferdora
*   Most Debian / RPM based OS which nodejs supports

### Node Versions

<figure class="wp-block-table">
   <table>
      <tbody>
         <tr>
            <td>Datatrucker</td>
            <td>Node Version</td>
         </tr>
         <tr>
            <td>1.0.x</td>
            <td>v12.x , v14.x</td>
         </tr>
      </tbody>
   </table>
</figure>

If your interested in the Kuberenetes version : redirect to the link : [Running on Kubernetes](https://www.datatrucker.io/knowledge-base/running-datatrucker-io-on-kubernetes/ "Running DataTrucker.io on Kubernetes")

### Backend database build

*   Datatrucker API server requires a Postgres Database.
*   A DB userid can connect to the above database with read/write access on the database
*   Note: if you are sharing the same Postgres instance with other apps, Datatrucker requires a dedicated/clean DB on that postgres instance.

### Datatrucker API / UI Server Dependencies

*   Two Ports are required ( 1 for API and 1 for UI , if necessary you can put them on separate machines )
*   Servers have access to npmjs.org or an equivalent NPM proxy/registry which all for npm install
*   The server has the following users defined
    *   A privileged Linux user-id to install globally scoped dependencies or temporary sudo access ( only required during installation )
    *   A Linux regular user-id to run node-js servers
*   Note: Download the software assumes accepting the License
*   Have a database setup, with a user-id to use as a backing store. See the next section for supported databases

### Supported backends databses

*   Postgres
    *   Adding more database soon

## Install the API server

### NPM dependencies

    # Verify if node is installed

    -> node -v
    v12.xxxxx or v14.xxxx or higher

### Create a folder to host the server

1.  SSH into the API VM (# For representation we use the user id trucker, this can be any user  and for representation we use the home directory as /home/trucker)
2.  create folder location -> mkdir /home/trucker/server
3.  move into the path -> cd /home/trucker/server
4.  download the source code from  latest release from [here](https://gitlab.com/datatruckerio/datatrucker-server/-/releases)
5.  once unzipped it should look like this

```
    trucker@trucker:~/server$ ls -la /home/trucker/server/
    total 28
    drwxrwxr-x  6 trucker trucker 4096 Apr 26 09:54 .
    drwxr-xr-x 32 trucker trucker 4096 Apr 26 09:52 ..
    drwxrwxr-x 11 trucker trucker 4096 Apr 30 17:07 datatrucker_api
    drwxrwxr-x  4 trucker trucker 4096 Apr 30 17:07 datatrucker_ui
    .
    .
    .
```

6. cd into datatrucker_api

### Install global dependencies

### 1) NPM global dependencies ( may require privileged user or sudo)

    npm install --registry 'https://registry.npmjs.org/' -g node-gyp pm2 knex   [ May require sudo to install global dependencies ]

    response: should look like this.
    + node-gyp@6.1.0
    + pm2@4.4.0
    + knex@0.21.0
    added 529 packages from 327 contributors in 15.832s

### 2) Install the application dependencies of the API server

    #move into the path where the server is unzipped
    cd ~/server/datatrucker_api/app

    #Make sure you have the package.json in the folder
    ~/server/datatrucker_api/app$ ls package.json 
    package.json

    # install npm dependencies 
    npm install --registry 'https://registry.npmjs.org/' 

### 3) Basic configuration

1.  Open the file datatrucker_api/app/config/server.config.json

    Edit the following
    port: prefer to use a port above 1024, so the server can be run without a privileged user ( default 8081)

2. Configure the database connection in datatrucker_api/app/config/db.config.json

```language
    {
          "type": "pg",
          "host": "localhost",
          "port": 30001,
          "username": "testuser",
          "password": "password",
          "database": "userdb",
          "PoolMin": 300,
          "PoolMax": 600
    }
```

### 4) Initialize the Database

    #move into the path where the server is unzipped
    cd /home/trucker/server

    #move into the App folder
    cd datatrucker_api/app

    #Initialize the database
    npm run migrate

    Response Sample:
    Using the environment: production
    Already at the base migration
    Using the environment: production
    Batch 1 run: 8 migrations

### 5) Start the API Server

```language
    #move into the path where the server is unzipped
    cd /home/trucker/server

    #move into the App folder
    cd datatrucker_api/app

    #start the server
    npm run trucker

    #you should see the pm2 start
    # to check status use
    pm2 status

    #to check realtime monitor use
    pm2 monit

    ##Troubleshooting
    run the commands to stop the app
    pm2 stop all
    pm2 delete all
    Check the logs folder to see what happened 
```

### 6) Initialize the admin user

##### Check the health

```language
    curl --location --request GET 'http://<URL>:<port>/api/v1/statuschecks/healthcheck'

    Response:
    {
        "reqCompleted": true,
        "date": "2021-01-02T15:28:18.900Z",
        "reqID": 1,
        "data": {
            "db": true,
            "cache": true,
            "dbinitialized": false
        },
        "serverID": "ServerHandler"
    }

```
##### Check the initialization status

```language
    curl --location --request GET 'http://<URL>:<port>/api/v1/statuschecks/is-intialized'

    Response:
    {
        "reqCompleted": true,
        "date": "2021-01-02T15:28:02.524Z",
        "reqID": 1,
        "data": {
            "isIntialized": false
        },
        "serverID": "ServerHandler"
    }
```

##### Initialize the server

```language
    curl --location --request POST 'http://<URL>:<port>/api/v1/statuschecks/intialize' \
    --header 'Content-Type: application/json' \
    --data-raw '{
    	"username": "godmode",       -> you admin user 
    	"password": "Secret@123",     -> your password
    }'

    Response:
    {
        "reqCompleted": true,
        "date": "2021-01-02T15:31:21.698Z",
        "reqID": 1,
        "data": "Admin user created",
        "serverID": "ServerHandler"
    }
```

##### Check the health

    curl --location --request GET 'http://<URL>:<port>/api/v1/statuschecks/healthcheck'

```language
    Response:
    {
        "reqCompleted": true,
        "date": "2021-01-02T15:35:48.112Z",
        "reqID": 1,
        "data": {
            "db": true,
            "cache": true,
            "dbinitialized": true
        },
        "serverID": "ServerHandler"
    }

```
## 7) Configure the UI Server

Note: The UI server is a development utility, this needs to exist on the developers laptop pointing to an API server .

Note: If you want to start UI in a different server  
the following sections need to be executed on the different server  
1) NPM dependencies  
2) Create a folder to host the server

Go into the UI folder

    #move into the path where the server is unzipped
    cd /home/trucker/

    #move into the UI folder
    cd datatrucker_ui/app

    #edit the file datatrucker_ui/.env
    edit "REACT_APP_APIURL" to point to the API URL
    edit "UI_PORT=" to point to the port of the UI

    #install global dependencies (may require sudo privileges )
    npm install --registry 'https://registry.npmjs.org/' -g pm2 

    #Install UI dependencies
    npm install

    #start the server
    npm run start

    #Congratulations, you can now access the installation via the browser on http://<uiserver>:<uiport>
    #and have finished the quick start guide.

    #For production hardening/ssl move into hardening guide. 
