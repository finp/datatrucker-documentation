<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->


Description
-----------

Datatrucker.IO API can be chained together to create a single combined API , Using the Chain API interface.

Assume you have 2 APIs, a database API to fetch comments from the DB (API1) and sentiment analysis API (API2) to analyze the statement.

Chain API allows APIs to be combined to create a new endpoint, i.e. User Input feeds API1, which in turn feeds as input to API 2, finally receiving the response.

UI
--

Access the DataTrucker URL via a browser

### Create a Chain-API

1.  Go to Build Chains API in the Sidebar > Chain APIs
2.  Fill the form to create the API
    1.  Resource Name: an arbitrary name to identify the resource
    2.  Method: The type of REST Call
    3.  Chain Definition: An Array( see sample in the API section or UI for an example

### Query the resource you created

    #############################
    ### query a chain endpoint
    #############################
    
    URL: /api/v1/chains/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
    		"<key>": "<value>",
                    "<key>": "<value>",
                    "<key>": "<value>"
    }
    
    Response: 200 OK
    {
       jsondata....
    }
    

API
---

### Create the API via REST

    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON):     
    {
      "resourcename":"ChainQuery",
      "type":"Chain",
      "restmethod":"GET",
      "chain":[
          {
            "stub":"sentiment2",
            "method":"POST",
            "datacontent":{
                "statement":"$request|$.msg"
            },
            "register":"sentimentresult"
          },
          {
            "stub":"echo1",
            "method":"GET",
            "datacontent":{
                "message":"$sentimentresult|$.calculation",
                "token":"$sentimentresult|$.tokens"
            }
          }
      ]
    }

Chain Module information
------------------------

The Chain Query module helps to combine APIs generated in the Datatrucker IO  into a single large API.

Sequence:
The Array entered in the key "chain" descriptor during the creation of the API, defines the sequence of execution.

Input: 
An identifier with "$request"  gets generated, and the data JSON key from the input query is mapped into "$request", this means that in the input of the API request if you pass "$request", it will be translated to request Body entering the chain

Note: if there is a chain -> body -> chain combo when the data enters the second chain $request is reset to the output of block

The format for using saved variables is 
$request|$.key  i.e before "|"  the id of the saved object to get, after the pipe the jsonpath value to be extracted from the saved object .  you can use the register key at any stage of the chain , to save new objects

Input generated during the chain:
Assume you have a chain, of 2 APIs ( similar to the example shown above )

    
    API1: SentimentAPI (registed as "sentiment") -> API2:  DB insert query
    
    In the REST Body of the request, if you send
    {
            "msg": "value1"
    }
    
    Then API 1, will receive a request to /api/v1/jobs/sentiment with body
    {
            "statement": "$request|$.key"    <- from request input  ( JSONPath ) . 
    }
    
    Then API 2, will receive {
     "data" :{
            "statement": "$sentiment|$.key"       <- from API1 response a json key called "sentiment" from API1 response  ( JSONPath ) 
        }
    }
    
    The response of API2  is sent back to the client. 


Please see the above example for better in the "Create the API via REST" section for better understanding
To retrieve the value from a saved object - we use the function "jp.value(obj, pathExpression[, newValue])" such that it returns as value of the datatype not as default array when using jp.query

JSON path parsing documentation  [here](https://www.npmjs.com/package/jsonpath), which  is helpful while developing new APIs

Set the log level to trace on your development instance, to see the data fetch on response on every request.  