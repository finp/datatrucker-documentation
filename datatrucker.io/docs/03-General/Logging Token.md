Description
-----------
<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->

To log into the application, we have two ways

1.  Login through the UI server (Designed for development and administration)
2.  Login through the API server ( Designed for day-to-day for operations )

UI
--

Access the DataTrucker UI URL via a browser

1.  We have two kinds of Authentication Keycloack and Local Access
    *   Keycloack Auth is available for externalized authentication
    *   Local user access management managed by data trucker backend database
2.  The user needs to input values to authenticate
    *   A username
    *   A password
    *   A tenant
    *   Any type of Authentication

![](../../static/diagrams/login/image.png)

API
---

To login into API server, fire the REST call against API server URL

    # For Local Authentication Only
    URL: /api/v1/login
    TYPE: POST
    BODY (JSON): 
    {
    	"username": "<username>",
    	"password": "<Complex password>",
    	"tenant": "<available tenant>"
    }
    
    Response: 200 OK
    {
        "username": "<username>",
        "status": true,
        "token": "<a JWT Token>"
    }

    # For KeyCloak Authentication
    URL: /api/v1/login-keycloak
    TYPE: POST
    BODY (JSON): 
    {
    	"username": "<username>",
    	"password": "<Complex password>",
    	"tenant": "<available tenant>"
    }
    
    Response: 200 OK
    {
        "username": "<username>",
        "status": true,
        "token": "<a JWT Token>"
    }