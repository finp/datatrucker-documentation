<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->
Description
-----------

Create an API to allow Redis to be accessible via REST calls.

Redis API is a simple service that allows applications to store and retrieve messages.

Several legacy applications

need a single location to store messages between stages, like a temporary cache. Redis API comes in handy when used as a messaging protocol between services using simple curl requests without a

massive Redis integration codebase.

UI
--

Access the DataTrucker URL via a browser

### Create a credentials

1.  Go to Credentials in the sidebar > Redis Credentials
2.  Scroll down to Credential Management
3.  Fill the form to create the connection
    1.  Credential name – an arbitrary value to identify the credential object
    2.  The hostname of the Redis. Note, make sure the API server(not the UI) has access into Redis
    3.  Port of the Redis Server
4.  Click on Create

### Make an API

### Create a redis set API

1.  Go to IOT API in the Sidebar > Redis SET APIs
2.  Fill the form to create the API
    1.  Resource Name: an arbitrary name to identify the resource
    2.  The Credential linked to the resources
    3.  Method: The type of REST Call
    4.  Source Type: Insert Data(SET) or Retrieve Data(GET)
    5.  Validation Regex of Input values, input sanitization before querying
        1.  Examples provided in the UI when you try to create a new API

### Create a redis get API

*   Go to Database API in the Sidebar > Redis SET APIs
*   Fill the form to create the API
    1.  Resource Name: an arbitrary name to identify the resource
    2.  The Credential linked to the resources
    3.  Method: The type of REST Call
    4.  Validation Regex of SQL Query, input sanitization before querying
        1.  Examples provided in the UI when you try to create a new API. [Documentation](/docs/General/Input%20Santization) :

### Query the resource you created

    #############################
    ### query a fetch endpoint
    #############################
    
    URL: /api/v1/jobs/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
    		"key": "<key>"
    }
    
    Response: 200 OK
    {
       jsondata....
    }
    
    #############################
    ### query a set endpoint
    #############################
    URL: /api/v1/jobs/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
    		"key": "<key>",
    		"value": "<value>",
    		"ttyl": <Integer, life(ttl) of the key-value pain in seconds" 
    }
    
    Response: 200 OK
    {
       jsondata....
    }

As a CRD in Openshift / Kubernetes
----------------------------------

For credentials use the the API below  to the management end point

    ---
    apiVersion: datatrucker.datatrucker.io/v1
    kind: DatatruckerFlow
    metadata:
      name: datatruckerflow-sample
    spec:
      Resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "256Mi"
          cpu: "500m"
      JobDefinitions:
      - resourcename: redis1
        type: IOT-Redis
        credentialname: redis1
        tenant: Admin
        restmethod: POST
        source_type: SET
        validations:
          type: object
          properties:
            key:
              type: string
              pattern: "^[a-zA-Z0-9_]*$"
              maxLength: 8
            value:
              type: string
              pattern: "^[a-zA-Z0-9_]*$"
              maxLength: 8
            ttyl:
              type: number
      Keys:
        configmap: placeholder
      Scripts:
        configmap: placeholder
      Type: Job
      DatatruckerConfig: datatruckerconfig-sample
      Replicas: 1
      API:
        name: api
        Image:
          repository: docker.io
          imageName: datatruckerio/datatrucker-api
          tagName: latest

      
API
---

### Create a credential via REST

    URL: /api/v1/credentials/
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
         "credentialname": "redisconnect",
         "type": "IOT-Redis",
         "hostname": "localhost",
         "port": 5433
    }
    
    Response: 201 OK

### Create the API via REST

    #############################
    ## create a Redis set-request 
    #############################
    
    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
            "resourcename": "redis1",
            "type": "IOT-Redis",
            "credentialname": "redis1",
            "restmethod": "POST",
            "source_type": "SET",
            "validations": {
              "type": "object",
              "properties": {
                "key": {
                  "type": "string",
                  "pattern": "^[a-zA-Z0-9_]*$",
                  "maxLength": 8
                },
                "value": {
                  "type": "string",
                  "pattern": "^[a-zA-Z0-9_]*$",
                  "maxLength": 8
                },
                "ttyl": {
                  "type": "number"
                }
              }
            }
          }
    
    Response: 201 OK
    
    
    #############################
    ## create a Redis fetch request 
    #############################
    
    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
            "resourcename": "redis2",
            "type": "IOT-Redis",
            "credentialname": "redis1",
            "restmethod": "GET",
            "source_type": "GET",
            "validations": {
              "type": "object",
              "properties": {
                "key": {
                  "type": "string",
                  "pattern": "^[a-zA-Z0-9_]*$",
                  "maxLength": 8
                }
              }
            }
          }
    
    Response: 201 OK