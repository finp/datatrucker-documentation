<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->
Description
-----------

Create an API to allows extracting the sentiment of a given string. The API provides a trainable model which enables different weights for specific keywords

UI
--

Access the DataTrucker URL via a browser

### Make an API

1.  Go to Utils API in the Sidebar > Sentiment API’s
2.  Fill the form to create the API
    1.  Resource Name: an arbitrary name to identify the resource
    2.  Method: The type of REST Call
    3.  Validation Regex of Input values is the input sanitization before executing the command. Arguments will be sent to API as an Array and will be space-separated. Every input argument needs to pass the regex as defined during the creation of the API. [Documentation for Validation](/docs/General/Input%20Santization)
    4.  Options: used to train models for sentiment analysis

### Query the resource you created

    URL: /api/v1/jobs/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
    		"statement": "paintings are good."
    }
    
    Response: 200 OK


As a CRD in Openshift / Kubernetes
----------------------------------

For credentials use the the API below  to the management end point

    ---
    apiVersion: datatrucker.datatrucker.io/v1
    kind: DatatruckerFlow
    metadata:
      name: datatruckerflow-sample
    spec:
      Resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "256Mi"
          cpu: "500m"
      JobDefinitions:
      - resourcename: sentiment2
        type: Util-Sentiment
        tenant: Admin
        restmethod: POST
        options:
          extras:
            cats: 5
            amazing: 2
            tasteless: -10
            Wanted: 10
        validations:
          type: object
          properties:
            statement:
              type: string
              pattern: "^[ A-za-z0-9.]*$"
              maxLength: 20
      Keys:
        configmap: placeholder
      Scripts:
        configmap: placeholder
      Type: Job
      DatatruckerConfig: datatruckerconfig-sample
      Replicas: 1
      API:
        name: api
        Image:
          repository: docker.io
          imageName: datatruckerio/datatrucker-api
          tagName: latest


      
API
---

### Create the API via REST

    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
        "resourcename": "sentiment2",
        "type": "Util-Sentiment",
        "restmethod": "POST",
        "options": {
                "extras": {
                  "cats": 5,
                  "amazing": 2,
                  "tasteless": -10,
                  "Wanted": 10
                }
            },
        "validations": {
            "type": "object",
            "properties": {
                "statement": {
                    "type": "string",
                    "pattern": "^[ A-za-z0-9.]*$",
                    "maxLength":20 
                }
            }
        }
    }
    
    Response: 201 OK