<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->

Description
-----------

The Echo API is a simple API that returns the data sent to the server.

There are several uses for Echo API,

*   such as formatting the response/requests when used on chains,
*   input validations,
*   placeholder APIs
*   Multi- API Aggregation on Chains

UI
--

Access the DataTrucker URL via a browser

1.  Go to Util API in the Sidebar > Echo API
2.  Fill the form to create the API
    1.  Resource Name: an arbitrary name to identify the resource
    2.  Method: The type of REST Call
    3.  Validation Regex of Input values is the input sanitization before executing the command. Arguments will be sent to API as an Array and will be space-separated. Every input argument needs to pass the regex as defined during the creation of the API.
    4.  [Documentation for Validation](/docs/General/Input%20Santization)


As a CRD in Openshift / Kubernetes
----------------------------------

For credentials use the the API below  to the management end point

    ---
    apiVersion: datatrucker.datatrucker.io/v1
    kind: DatatruckerFlow
    metadata:
      name: datatruckerflow-sample
    spec:
      Resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "256Mi"
          cpu: "500m"
      JobDefinitions:
      - resourcename: echo1
        tenant: Admin
        type: Util-Echo
        restmethod: GET
        validations:
          type: object
          properties:
            voila:
              type: string
              enum:
              - 'true'
              - 'false'
      Keys:
        configmap: placeholder
      Scripts:
        configmap: placeholder
      Type: Job
      DatatruckerConfig: datatruckerconfig-sample
      Replicas: 1
      API:
        name: api
        Image:
          repository: docker.io
          imageName: datatruckerio/datatrucker-api
          tagName: latest
      
API
---

    #############################
    ## create a Echo API
    #############################
    
    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
            "resourcename": "echo1",
            "type": "Util-Echo",
            "restmethod": "GET",
            "validations":{
            "type": "object",
            "properties": {
                "voila": {
                    "type": "string",
                    "enum": ["true","false"]
                }
            }
        }
    }
    
    Response: 201 OK