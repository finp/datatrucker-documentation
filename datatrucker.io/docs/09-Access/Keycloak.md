<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->
Setup Configuration
-------------------

Assumption:

1.  A working and reachable (from the api server) Keycloak setup
2.  A Realm defined

#### Create a realm

note: preferable without spaces

![](../../static/diagrams/keycloak/image-1.png)

#### create a client

![](../../static/diagrams/keycloak/image-2.png)

Set the Client to confidential so you can get access to the credential tab .

The credential tab provides, client\_id and client\_secret

You can get the publickey verifier from the Realm settings

![](../../static/diagrams/keycloak/image-3-1024x210.png)

#### Configuring a Group

Create a "datatrucker group" type key cloak : Sample below

![](../../static/diagrams/keycloak/image-5.png)

Create a role with same name in keycloak

![](../../static/diagrams/keycloak/image-6.png)

Add users to the role on keycloak

![](../../static/diagrams/keycloak/image-7-1024x286.png)

```language
Note:Important
**Create a Keycloak Role ** with the same name as the Group created in Datatrucker , and add the group into the role
```

#### Test the users authentication via API to key cloak,

![](../../static/diagrams/keycloak/image-8.png)

#### Configure the crypto.config.json file on the API server

         "keycloak": {            
                "realm" : "trucker",     -----> Realm created
                "client_id": "nodejs",       -----> Client created
                "url": "http://172.28.24.164:30015",     -----> keycloak URL
                "client_secret": "8fa4d2ee-baed-4209-adeb-8926127df2ea",     -----> Realm created
                "jwt_publickey_verifier" : "keycloak.key"          -----> Realm created 
          }

Once the above is configured, start the api server and test the authentication

![](../../static/diagrams/keycloak/image-9.png)