<!---
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
License : https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt
Attribution: DataTrucker.io Inc , Ontario, Canada
-->
Description
-----------

Create an API that can trigger Shell commands on the API server.  

Note: The Shell Command runs ssh commands in the context of the API server user, care must be taken to prevent access to server files

UI
--

Access the DataTrucker URL via a browser

### Create a Shell API

1.  Go to File API in the Sidebar > Shell APIs
2.  Fill the form to create the API
    1.  Resource Name: an arbitrary name to identify the resource
    2.  Method: The type of REST Call
    3.  Script: the SSH command
    4.  Validation Regex of Input values, input sanitization before querying. Note: the arguments are passed in an array and all arguments need to pass the input sanitization.
        1.  Examples provided in the UI when you try to create a new API , [Validation Documents](/docs/General/Input%20Santization)

### Query the resource you created

    #############################
    ### Fire the commands
    #############################
    
    URL: /api/v1/jobs/<resource name>
    TYPE: <method defined>
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY:
    {
                        "args" :['-la']
    }
    
    Response: 200 OK
    {
       jsondata....
    }
    

As a CRD in Openshift / Kubernetes
----------------------------------

For credentials use the the API below  to the management end point

    ---
    apiVersion: datatrucker.datatrucker.io/v1
    kind: DatatruckerFlow
    metadata:
      name: datatruckerflow-sample
    spec:
      Resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "256Mi"
          cpu: "500m"
      JobDefinitions:
      - resourcename: scriptshell1
        type: Script-Shell
        credentialname: sshlocalconnect
        tenant: Admin
        restmethod: GET
        script: whoami
        validations:
          type:
          - object
          - 'null'
          properties:
            args:
              type: string
              pattern: "^[-a-z0-9]*$"
              maxLength: 8
      Keys:
        configmap: placeholder
      Scripts:
        configmap: placeholder
      Type: Job
      DatatruckerConfig: datatruckerconfig-sample
      Replicas: 1
      API:
        name: api
        Image:
          repository: docker.io
          imageName: datatruckerio/datatrucker-api
          tagName: latest

      
API
---

### Create the API via REST

    #############################
    ## create a  command request 
    #############################
    
    URL: /api/v1/resources
    TYPE: POST
    HEADER: 
    Authorization: "Bearer <JWT Token>"
    BODY (JSON): 
    {
            "resourcename": "scriptshell1",
            "type": "Script-Shell",
            "credentialname": "sshlocalconnect",
            "restmethod": "GET",
            "script": "whoami",
            "validations": {
              "type": ["object", "null"],
              "properties": {
                "args": {
                    "type": "string",
                    "pattern": "^[-a-z0-9]*$",
                    "maxLength": 8
                }
              }
            }
          }
    
    Response: 201 OK