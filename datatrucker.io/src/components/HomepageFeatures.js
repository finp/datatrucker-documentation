import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'API Growth',
    Svg: require('../../static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        Today most companies take 15-20k$ with an average development time of 15-17 days to build an API. With DataTrucker, build production quality APIs in 10 min?
      </>
    ),
  },
  {
    title: 'Grow from a single API to 100’s',
    Svg: require('../../static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        An API first product needs hundreds of APIs to make it complete. With DataTrucker, publishing hundreds of API while leveraging popular source control tooling wil be a cinch
      </>
    ),
  },

  {
    title: 'Security First',
    Svg: require('../../static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        Applying a standard security framework on each API may get complex. With Datatrucker , developer is focused on business logic - the installation provides default security wrapper
      </>
    ),
  },



];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
